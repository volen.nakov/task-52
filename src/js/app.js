import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  document.addEventListener("click", () => {
    for (let i = 0; i < 5; i++) {
      let container = document.createElement('div')
      container.classList.add('message')
      let text = document.createTextNode('test'+i)
      container.appendChild(text)
      document.body.appendChild(container)
    }
  })
  const button = document.querySelector(".button");
  button.addEventListener("click", () => {
    alert("💣");
  });
});
